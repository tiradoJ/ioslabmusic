//
//  LatTunesTests.swift
//  LatTunesTests
//
//  Created by Jesús Tirado on 11/9/18.
//  Copyright © 2018 com.tiradoJ. All rights reserved.
//

import XCTest
@testable import LatTunes

class LatTunesTests: XCTestCase {

    override func setUp() {
        let session = Session.sharedInstance
        session.token = nil
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCorrectLogin() {
        XCTAssertTrue(User.login(userName: "iOSLab", password: "verysecurepassword"))
    }
    
    func testWrongLogin() {
        XCTAssertFalse(User.login(userName: "jesus", password: "tirado"))
    }

    func testSaveSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "verysecurepassword")
        XCTAssertNotNil(session.token)
    }
    
    func testWrongSaveSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLabs", password: "verysecurepassword")
        XCTAssertNil(session.token)
    }

    func testExpectedToken() {
        let _ = User.login(userName: "iOSLab", password: "verysecurepassword")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890", "Token should match")
    }
    
    
//    func testWrongExpectedToken() {
//        let _ = User.login(userName: "iOSLabs", password: "verysecurepassword")
//        let session = Session.sharedInstance
//        XCTAssertNotEqual(session.token, "1234567891", "Token should not match")
//    }

    func testThowsError() {
        XCTAssertThrowsError(try User.fetchSongs())
    }
    
    func testMusicSongs() {
        var resultSong: [Song] = []
        let promise = expectation(description: "songs downloaded")
        Music.fetchSongs{(songs) in
            resultSong = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSong.count, 0)
    }
}

