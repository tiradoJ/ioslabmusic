//
//  ViewController.swift
//  LatTunes
//
//  Created by Jesús Tirado on 11/9/18.
//  Copyright © 2018 com.tiradoJ. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        guard let userName = username.text else { return }
        guard let password = passwordText.text else { return }
        if User.login(userName: userName, password: password) {
            performSegue(withIdentifier: "showTableView", sender: self)
        } else {
            //alert
        }
    }
}

